## Ce Readme explique comment comprendre ce répertoire Git

Le travail de notre groupe se trouve entièrement sur la branche main du répertoire, il comporte:
- le travail de chacun des membres du groupe (ATTENTION: les prénoms indiqués n'indiquent pas qui est l'auteur du fichier mais qui l'a posté)
- les contenus des fichiers sont les suivants: classification par svm & pca, lstm (réseaux de neurones), gradient booster 
- les CSV ont également été uploadés dans dans les fichiers code_emmanuel et code_chenghao
