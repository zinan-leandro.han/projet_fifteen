import os
import glob
import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from sklearn.model_selection import train_test_split

# Chemin vers les fichiers CSV filtrés
chemins_csv_filtres = glob.glob("/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_filtres" + "/*.csv")

df_labels = pd.read_csv("/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/labels.csv")

# Convertir les étiquettes en type approprié
df_labels['label'] = df_labels['label'].astype(int)

#-------------------------------------------------------------------------------------------

# Filtrer les DataFrames dont la dimension est différente de [x, 3]
train_csv_paths = []
val_csv_paths = []
dataframes_rejetes = []

for chemin_csv in chemins_csv_filtres:
    df = pd.read_csv(chemin_csv)
    if df.shape == (21, 9):
        if np.random.rand() < 0.8:
            train_csv_paths.append(chemin_csv)
        else:
            val_csv_paths.append(chemin_csv)
    else:
        dataframe_id = os.path.basename(chemin_csv).split(".")[0]
        dataframes_rejetes.append(dataframe_id)

# Affichage des identifiants des DataFrames rejetés
print("Identifiants des DataFrames rejetés :")
for dataframe_id in dataframes_rejetes:
    print(dataframe_id)

# Définition des caractéristiques à utiliser
df_sample = pd.read_csv(train_csv_paths[0])
features = df_sample.columns.tolist()

#-------------------------------------------------------------------------------------------
# Classe permettant de charger les données sous forme de tensors
class CustomDataset(Dataset):
    def __init__(self, csv_paths, labels):
        self.csv_paths = csv_paths
        self.labels = labels

    def __len__(self):
        return len(self.csv_paths)

    def __getitem__(self, index):
        csv_path = self.csv_paths[index]
        data = pd.read_csv(csv_path)[features].values
        label = self.labels[index]

        return data, label
    
train_dataset = CustomDataset(train_csv_paths, df_labels['label'].values)
val_dataset = CustomDataset(val_csv_paths, df_labels['label'].values)

# Définition des paramètres du modèle
input_size = len(features)
hidden_size = 32
output_size = 1
num_layers = 2
batch_size = 16
num_epochs = 15
learning_rate = 0.001

#-------------------------------------------------------------------------------------------
# Définition du modèle LSTM
class LSTM(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, num_layers):
        super(LSTM, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        batch_size = x.size(0)  # Récupérer la taille du batch
        h0 = torch.zeros(self.num_layers, batch_size, self.hidden_size).float().to(device)  # Ajuster la dimension de h0 et convertir en float
        c0 = torch.zeros(self.num_layers, batch_size, self.hidden_size).float().to(device)  # Ajuster la dimension de c0 et convertir en float

        out, _ = self.lstm(x.float(), (h0, c0))  # Convertir les données d'entrée en float
        out = self.fc(out[:, -1, :])

        return out

#-------------------------------------------------------------------------------------------
# Initialisation du modèle
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = LSTM(input_size, hidden_size, output_size, num_layers).to(device)

# Définition de la fonction de perte et de l'optimiseur
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Création des chargeurs de données
train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

# Entraînement du modèle
total_step = len(train_loader)
for epoch in range(num_epochs):
    for i, (inputs, labels) in enumerate(train_loader):
        inputs = inputs.to(device)
        labels = labels.to(device)
        # print(inputs)
        # print(labels)

        # Passe les données en avant (aux couches suivantes du modèle)
        outputs = model(inputs)
        loss = criterion(outputs.squeeze(), labels.float())

        # Backward et optimization
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if (i + 1) % 10 == 0:
            print(f"Epoch [{epoch + 1}/{num_epochs}], Loss: {loss.item():.4f}")

# Évaluation du modèle
model.eval()
with torch.no_grad():
    correct = 0
    total = 0
    for inputs, labels in val_loader:
        inputs = inputs.to(device)
        labels = labels.to(device)

        outputs = model(inputs)
        _, predicted = torch.max(outputs.data, 1)  
        total += labels.size(0)
        correct += torch.sum(torch.eq(predicted, labels)).item()

    accuracy = correct / total
    print(f"Validation Accuracy: {accuracy:.2%}")


