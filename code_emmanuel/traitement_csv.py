import pandas as pd
import glob
import os
# from code_seuils import seuil

# Chemin vers CS dataset
dossier_csv = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/trips"
dossier_reduit = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_reduits"
dossier_filtres = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_filtres"

# Obtient la liste de tous les CSV de CS dataset
chemins_csv = glob.glob(dossier_csv + "/*.csv")

#-------------------------------------------------------------------------------------------
# Créer les dossiers pour les fichiers réduits et filtrés s'ils n'existent pas
if not os.path.exists(dossier_reduit):
    os.makedirs(dossier_reduit)

if not os.path.exists(dossier_filtres):
    os.makedirs(dossier_filtres)
#-------------------------------------------------------------------------------------------

longueur_csv = 5  # Nombre de lignes souhaité dans les CSV résultants

features_selectionnees = ['AYGX_min', 'AZ_mean','AYGX_mean','AYGX_std','AZ_mean','GX_std','Gnorm_max','Gnorm_median', 'timestamp']

for chemin_csv in chemins_csv:
    df = pd.read_csv(chemin_csv)

    # On sélectionne uniquement les 3 features identifiées
    df_reduit = df[features_selectionnees]

    nom_fichier = os.path.basename(chemin_csv)

    chemin_fichier_reduit = os.path.join(dossier_reduit, nom_fichier)

    # Enregistrer les données réduites dans un nouveau fichier CSV
    df_reduit.to_csv(chemin_fichier_reduit, index=False)

    df_reduit = pd.read_csv(chemin_fichier_reduit)

    # Trouver l'indice du maximum en valeur absolue de AYGX_mean
    indice_max_abs = df_reduit["AYGX_min"].abs().idxmax()

    borne_inf = max(indice_max_abs - (longueur_csv // 2), 0)
    borne_sup = borne_inf + longueur_csv

    # Filtre les données autour de l'indice de l'instant de la chute présumée par nos soins (grâce à AYGX)
    df_filtre = df_reduit.iloc[borne_inf:borne_sup]

    # Obtenir l'identifiant du fichier CSV à partir du nom du fichier
    identifiant = nom_fichier.split(".csv")[0]

    # Définir le chemin pour enregistrer le nouveau fichier CSV filtré
    chemin_fichier_filtre = os.path.join(dossier_filtres, f"{identifiant}.csv")

    # Enregistrer les données filtrées dans un nouveau fichier CSV
    df_filtre.to_csv(chemin_fichier_filtre, index=False)
