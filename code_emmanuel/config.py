import os
import pandas as pd
import glob
import matplotlib.pyplot as plt

BASE_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
DATASET_DIRECTORY = os.path.join(BASE_DIRECTORY, "CS_dataset")
TRIPS_DIRECTORY = os.path.join(DATASET_DIRECTORY, "trips")

def get_label_dataframe():
    return pd.read_csv(os.path.join(DATASET_DIRECTORY,"labels.csv"))

def get_trip_dataframe(trip_id: str):
    return pd.read_csv(os.path.join(TRIPS_DIRECTORY, f"{trip_id}.csv"))

# print(get_trip_dataframe('cgrabe7be75spfm9rktg'))
df_labels = get_label_dataframe()

#----------------------------------------------------------------------------

print(get_label_dataframe())
df = get_trip_dataframe('cgnk8tvo61i2hp2g90b0')
features = df.columns.tolist()
print(features)